from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

import logging
import time, requests, json
from emoji import emojize

from telegram.ext import BaseFilter


commands = {}
masters = ['nadimulabrar']

class NewGroupAdd(BaseFilter):
    def filter(self, message):
        return len(message.new_chat_members) != 0 and message.new_chat_members[0].username == 'group_butler_stb_bot' #change bot username here
    
class NewChatMembers(BaseFilter):
    def filter(self, message):
        return len(message.new_chat_members) != 0 and message.new_chat_members[0].username != 'group_butler_stb_bot' #change bot username here

    
test_filter = NewGroupAdd()
greet_filter = NewChatMembers()



def check(bot, update):
    global masters, commands
    if update.message.from_user.username not in masters:
        update.message.reply_text('Hi! I\'m a Smart Butler! Sorry i\'m leaving! I can only be added by My Admins!')
        bot.leaveChat(update.message.chat_id)
    else:
        chatid = str(update.message.chat_id)
        if chatid not in commands:
            commands[chatid] = {}
            with open('commands.json', 'w') as outfile:
                json.dump(commands, outfile)
        update.message.reply_text('Hi! I\'m a Smart Butler! I\'m here to help')
        
def greet(bot, update):
    global masters, commands
    if update.message.chat_id < 0:
        update.message.reply_text('Everyone! Please welcome ' + update.message.new_chat_members[0].first_name + ' ' + update.message.new_chat_members[0].last_name)
    
    
def start_handler(bot, update):
    global masters, commands
    if update.message.chat_id > 0:
        update.message.reply_text('Hi! I\'m a Smart Butler! I only function in a group')
    else:
        update.message.reply_text('Hi! I\'m a Smart Butler! I\'m here to help')
    
def save_handler(bot, update):
    global masters, commands
    if update.message.chat_id < 0:
        chatid = str(update.message.chat_id)
        msg = update.message.text
        key = msg.split()[1]
        if key in commands[chatid]:
            update.message.reply_text('Same commands already exists! Try something different')
        else:
            cnt = 0
            for i in range(len(msg)):
                if msg[i] == ' ' or msg[i] == '\n':
                    cnt = cnt + 1
                    if(cnt == 2):
                        msg = msg[i+1:]
                        update.message.reply_text(msg)
                        break
                    
            commands[chatid][key] = msg
            with open('commands.json', 'w') as outfile:
                    json.dump(commands, outfile)
            update.message.reply_text('Command saved!')
            
    
def remove_handler(bot, update, args):
    global masters, commands
    if update.message.chat_id < 0:
        chatid = str(update.message.chat_id)
        key = args[0]
        if key not in commands[chatid]:
            update.message.reply_text('The commands does not exist!')
        else:
            commands[chatid].pop(key, None)
            with open('commands.json', 'w') as outfile:
                    json.dump(commands, outfile)
            update.message.reply_text('Command removed!')
    
    
def show_handler(bot, update, args):
    global masters, commands
    if update.message.chat_id < 0:
        chatid = str(update.message.chat_id)
        key = args[0]
        if key not in commands[chatid]:
            update.message.reply_text('The commands does not exist!')
        else:
            update.message.reply_text(commands[chatid][key])

def error_handler(bot, update, error):
    logger.warning('Update "%s" caused error "%s"', update, error)

    
def main():
    global commands, test_filter, greet_filter
    commands = json.loads(open('commands.json').read())
    updater = Updater("BOT Token") #change token here
    dp = updater.dispatcher
    dp.add_handler(MessageHandler(test_filter, check))
    dp.add_handler(MessageHandler(greet_filter, greet))
    dp.add_handler(CommandHandler('start', start_handler))
    dp.add_handler(CommandHandler('save', save_handler))
    dp.add_handler(CommandHandler('remove', remove_handler, pass_args = True))
    dp.add_handler(CommandHandler('show', show_handler, pass_args = True))
    
    dp.add_error_handler(error_handler)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()

