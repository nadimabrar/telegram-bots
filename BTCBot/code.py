from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

import logging
import time, requests, json
from emoji import emojize


sub_list = []

def start_handler(bot, update):
    if update.message.chat_id not in subscribers:
        subscribers.append(update.message.chat_id)
        with open('subscribers.json', 'w') as outfile:
            json.dump(subscribers, outfile)

def error_handler(bot, update, error):
    logger.warning('Update "%s" caused error "%s"', update, error)  

def callback_minute(bot, job):
    try:
        response = requests.get('https://bittrex.com/api/v1.1/public/getmarketsummary?market=usdt-btc')
        data = json.loads(response.text)['result'][0]['Last']
        for chat_id in subscribers:
            try:
                bot.send_message(chat_id=chat_id, text=emojize(':moneybag: {:.8f}'.format(data), use_aliases=True))
            except:
                pass
    except:
        pass

def main():
    global subscribers
    subscribers = json.loads(open('subscribers.json').read())
    
    updater = Updater("BOT_Token")
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start_handler))
    dp.add_error_handler(error_handler)
    
    updater.job_queue.run_repeating(callback_minute, interval=60, first=0)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
