import matplotlib
matplotlib.use('Agg')
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

import logging
import time, requests, json
from emoji import emojize

import time, requests, json
import datetime
import numpy as np
from matplotlib.dates import date2num
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
from matplotlib.finance import candlestick_ohlc
import pylab
import os

masters = ["nadimulabrar", "FacebookAgent"]
matplotlib.rcParams.update({'font.size': 9})

def rsiFunc(prices, n=14):
    deltas = np.diff(prices)
    seed = deltas[:n+1]
    up = seed[seed>=0].sum()/n
    down = -seed[seed<0].sum()/n
    rs = up/down
    rsi = np.zeros_like(prices)
    rsi[:n] = 100. - 100./(1.+rs)

    for i in range(n, len(prices)):
        delta = deltas[i-1]

        if delta>0:
            upval = delta
            downval = 0.
        else:
            upval = 0.
            downval = -delta

        up = (up*(n-1) + upval)/n
        down = (down*(n-1) + downval)/n

        rs = up/down
        rsi[i] = 100. - 100./(1.+rs)

    return rsi

def movingaverage(values,window):
    weigths = np.repeat(1.0, window)/window
    smas = np.convolve(values, weigths, 'valid')
    return smas # as a numpy array


def ExpMovingAverage(values, window):
    weights = np.exp(np.linspace(-1., 0., window))
    weights /= weights.sum()
    a =  np.convolve(values, weights, mode='full')[:len(values)]
    a[:window] = a[window]
    return a


def computeMACD(x, slow=26, fast=12):
    emaslow = ExpMovingAverage(x, slow)
    emafast = ExpMovingAverage(x, fast)
    return emaslow, emafast, emafast - emaslow


def graphData(coin, time_in, cnt, save_name, width, MA1 = 12, MA2 = 24):
    response = requests.get('https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName=' + coin + '&tickInterval=' + time_in)
    data = json.loads(response.text)['result']

    date = []
    closep = []
    highp = []
    lowp = []
    openp = []
    volume = []

    for i in range(-cnt, 0):
        date.append(date2num(datetime.datetime.strptime(data[i]['T'], '%Y-%m-%dT%H:%M:%S')))
        closep.append(data[i]['C'])
        highp.append(data[i]['H'])
        lowp.append(data[i]['L'])
        openp.append(data[i]['O'])
        volume.append(data[i]['V'])

    date = np.array(date)
    closep = np.array(closep)
    highp = np.array(highp)
    lowp = np.array(lowp)
    openp = np.array(openp)
    volume = np.array(volume)

    x = 0
    y = len(date)
    newAr = []
    while x < y:
        appendLine = date[x],openp[x],highp[x],lowp[x],closep[x],volume[x]
        newAr.append(appendLine)
        x+=1

    Av1 = movingaverage(closep, MA1)
    Av2 = movingaverage(closep, MA2)

    SP = len(date[MA2-1:])

    fig = plt.figure(facecolor='#07000d', figsize = (9,6))

    ax1 = plt.subplot2grid((6,4), (1,0), rowspan=4, colspan=4, axisbg='#07000d')
    candlestick_ohlc(ax1, newAr[-SP:], width = width * cnt, colorup='#53c156', colordown='#ff1717')

    Label1 = str(MA1)+' SMA'
    Label2 = str(MA2)+' SMA'

    ax1.plot(date[-SP:],Av1[-SP:],'#e1edf9',label=Label1, linewidth=1.5)
    ax1.plot(date[-SP:],Av2[-SP:],'#4ee6fd',label=Label2, linewidth=1.5)

    ax1.grid(True, color='w')
    ax1.xaxis.set_major_locator(mticker.MaxNLocator(10))
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H-%M'))
    ax1.yaxis.label.set_color("w")
    ax1.spines['bottom'].set_color("#5998ff")
    ax1.spines['top'].set_color("#5998ff")
    ax1.spines['left'].set_color("#5998ff")
    ax1.spines['right'].set_color("#5998ff")
    ax1.tick_params(axis='y', colors='w')
    plt.gca().yaxis.set_major_locator(mticker.MaxNLocator(prune='upper'))
    ax1.tick_params(axis='x', colors='w')
    plt.ylabel('Price and Volume')

    maLeg = plt.legend(loc=9, ncol=2, prop={'size':7},
               fancybox=True, borderaxespad=0.)
    maLeg.get_frame().set_alpha(0.4)
    textEd = pylab.gca().get_legend().get_texts()
    pylab.setp(textEd[0:5], color = 'w')

    volumeMin = 0

    ax0 = plt.subplot2grid((6,4), (0,0), sharex=ax1, rowspan=1, colspan=4, axisbg='#07000d')
    rsi = rsiFunc(closep)
    rsiCol = '#c1f9f7'
    posCol = '#386d13'
    negCol = '#8f2020'

    ax0.plot(date[-SP:], rsi[-SP:], rsiCol, linewidth=1.5)
    ax0.axhline(70, color=negCol)
    ax0.axhline(30, color=posCol)
    ax0.fill_between(date[-SP:], rsi[-SP:], 70, where=(rsi[-SP:]>=70), facecolor=negCol, edgecolor=negCol, alpha=0.5)
    ax0.fill_between(date[-SP:], rsi[-SP:], 30, where=(rsi[-SP:]<=30), facecolor=posCol, edgecolor=posCol, alpha=0.5)
    ax0.set_yticks([30,70])
    ax0.yaxis.label.set_color("w")
    ax0.spines['bottom'].set_color("#5998ff")
    ax0.spines['top'].set_color("#5998ff")
    ax0.spines['left'].set_color("#5998ff")
    ax0.spines['right'].set_color("#5998ff")
    ax0.tick_params(axis='y', colors='w')
    ax0.tick_params(axis='x', colors='w')
    plt.ylabel('RSI')

    ax1v = ax1.twinx()
    ax1v.fill_between(date[-SP:],volumeMin, volume[-SP:], facecolor='#00ffe8', alpha=.4)
    ax1v.axes.yaxis.set_ticklabels([])
    ax1v.grid(False)
    ax1v.set_ylim(0, 3*volume.max())
    ax1v.spines['bottom'].set_color("#5998ff")
    ax1v.spines['top'].set_color("#5998ff")
    ax1v.spines['left'].set_color("#5998ff")
    ax1v.spines['right'].set_color("#5998ff")
    ax1v.tick_params(axis='x', colors='w')
    ax1v.tick_params(axis='y', colors='w')
    ax2 = plt.subplot2grid((6,4), (5,0), sharex=ax1, rowspan=1, colspan=4, axisbg='#07000d')
    fillcolor = '#00ffe8'
    nslow = 26
    nfast = 12
    nema = 9
    emaslow, emafast, macd = computeMACD(closep)
    ema9 = ExpMovingAverage(macd, nema)
    ax2.plot(date[-SP:], macd[-SP:], color='#4ee6fd', lw=2)
    ax2.plot(date[-SP:], ema9[-SP:], color='#e1edf9', lw=1)
    ax2.fill_between(date[-SP:], macd[-SP:]-ema9[-SP:], 0, alpha=0.5, facecolor=fillcolor, edgecolor=fillcolor)

    plt.gca().yaxis.set_major_locator(mticker.MaxNLocator(prune='upper'))
    ax2.spines['bottom'].set_color("#5998ff")
    ax2.spines['top'].set_color("#5998ff")
    ax2.spines['left'].set_color("#5998ff")
    ax2.spines['right'].set_color("#5998ff")
    ax2.tick_params(axis='x', colors='w')
    ax2.tick_params(axis='y', colors='w')
    plt.ylabel('MACD', color='w')
    ax2.yaxis.set_major_locator(mticker.MaxNLocator(nbins=5, prune='upper'))
    for label in ax2.xaxis.get_ticklabels():
        label.set_rotation(45)

    plt.suptitle(coin.upper(),color='w')
    plt.setp(ax0.get_xticklabels(), visible=False)
    plt.setp(ax1.get_xticklabels(), visible=False)


    plt.subplots_adjust(left=.09, bottom=.14, right=.94, top=.95, wspace=.20, hspace=0)
    fig.savefig(save_name + '.png',facecolor=fig.get_facecolor())


subscribers = [] 
admins = []


def start_handler(bot, update):
    update.message.reply_text('Hi ' + update.message.from_user.first_name + '! My name is S.T.B Assistent Bot! I\'m a cryptocurrency price bot\n\n'
                                'Send /help to know what you can do with me.\n\n'
                                'Don\'t forget to /subscribe if you want to get notifications from us regulerly! '
                                'Don\'t worry! you can /unsubscribe any time you want!')

def help_handler(bot, update):
    update.message.reply_text('This is some text to help you!')

def b_handler(bot, update, args):
    response = requests.get('https://bittrex.com/api/v1.1/public/getmarketsummary?market=' + args[0] + '-' + args[1])
    data = json.loads(response.text)['result'][0]
    update.message.reply_text('Bittrex: ' + str(data['MarketName']) + '\n\n'
                              'Bid: ' + '{:.8f}'.format(data['Bid']) + '\n'
                              'Ask: ' + '{:.8f}'.format(data['Ask']) + '\n'
                              'Last: ' + '{:.8f}'.format(data['Last']) + '\n\n'
                              'Volume: ' + '{:.8f}'.format(data['BaseVolume']) + '\n\n'
                              '24h High: ' + '{:.8f}'.format(data['High']) + '\n'
                              '24h Low: ' + '{:.8f}'.format(data['Low']) + '\n\n'
                              'OpenBuyOrd: ' + '{:.0f}'.format(data['OpenBuyOrders']) + '\n'
                              'OpenSellOrd: ' + '{:.0f}'.format(data['OpenSellOrders']) + '\n\n'
                              'Check out https://bittrex.com/Market/Index?MarketName=' + args[0] + '-' + args[1] + ' for more!')

def broadcast(bot, update, args):
    if (update.message.from_user.username in admins) or (update.message.from_user.username in masters):
        msg = ''
        for arg in args:
            msg += arg + ' '
        for chat_id in subscribers:
            bot.send_message(chat_id=chat_id, text=msg)

def subscribe_handler(bot, update):
    if update.message.chat_id not in subscribers:
        subscribers.append(update.message.chat_id)
        with open('subscribers.json', 'w') as outfile:
            json.dump(subscribers, outfile)
        update.message.reply_text('Thank you so much for subscribing!')
    else:
        update.message.reply_text('You are already subscribed! Stay tuned')
        
def unsubscribe_handler(bot, update):
    if update.message.chat_id in subscribers:
        subscribers.remove(update.message.chat_id)
        with open('subscribers.json', 'w') as outfile:
            json.dump(subscribers, outfile)
        update.message.reply_text(emojize('I\'m am sorry to see you go :disappointed:. Please let me know what went wrong.', use_aliases=True))
    else:
        update.message.reply_text('You are not subscribed! Please subscribe before you unsubscribe.')

def bc_handler(bot, update, args):
    if len(args) == 2:
        graphData(args[0] + '-' + args[1], 'thirtyMin', 48, str(update.message.chat_id), .00035)
    else:
        graphData(args[0] + '-' + args[1], 'oneMin', int(args[2]), str(update.message.chat_id), .000005)
    bot.send_photo(chat_id=update.message.chat_id, photo=open(str(update.message.chat_id)+'.png', 'rb'))
    os.remove(str(update.message.chat_id)+'.png')

def add_admin(bot, update, args):
    if (update.message.from_user.username in admins) or (update.message.from_user.username in masters):
        if args[0] not in admins:
            admins.append(args[0])
            with open('admins.json', 'w') as outfile:
                json.dump(admins, outfile)
            update.message.reply_text(args[0] + ' is an admin now!')
        else:
            update.message.reply_text(args[0] + 'is already an admin!')
            
def remove_admin(bot, update, args):
    if (update.message.from_user.username in admins) or (update.message.from_user.username in masters):
        if args[0] in admins:
            admins.remove(args[0])
            with open('admins.json', 'w') as outfile:
                json.dump(admins, outfile)
            update.message.reply_text(args[0] + 'is not an admin anymore!')
        else:
            update.message.reply_text(args[0] + 'is not an admin!')
        

def show_admins(bot, update):
    if (update.message.from_user.username in admins) or (update.message.from_user.username in masters):
        msg = ''
        for admin in admins:
            msg += admin + '\n'
        update.message.reply_text(msg)
            
def error_handler(bot, update, error):
    logger.warning('Update "%s" caused error "%s"', update, error)

    

def main():
    global subscribers, admins
    subscribers = json.loads(open('subscribers.json').read())
    admins = json.loads(open('admins.json').read())
    updater = Updater("Bot Token")
    dp = updater.dispatcher
    
    dp.add_handler(CommandHandler('start', start_handler))
    dp.add_handler(CommandHandler('subscribe', subscribe_handler))
    dp.add_handler(CommandHandler('unsubscribe', unsubscribe_handler))
    dp.add_handler(CommandHandler('help', help_handler))
    dp.add_handler(CommandHandler('b', b_handler, pass_args = True))
    dp.add_handler(CommandHandler('bc', bc_handler, pass_args = True))
    dp.add_handler(CommandHandler('addadmin', add_admin, pass_args = True))
    dp.add_handler(CommandHandler('removeadmin', remove_admin, pass_args = True))
    dp.add_handler(CommandHandler('showadmins', show_admins))
    dp.add_handler(CommandHandler('broadcast', broadcast, pass_args = True))
    
    dp.add_error_handler(error_handler)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
